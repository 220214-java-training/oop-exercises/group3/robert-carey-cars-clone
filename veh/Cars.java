package veh;

public class Cars {



	public static void main(String[] args) {
		Bmw b = new Bmw("Blue", "Bmw", "i8", "luxury");
		Honda h = new Honda("White", "Honda", "Accord", "Economy");
		Ford f = new Ford("Black", "Ford", "Mustang", "Sports");
		Delorean d = new Delorean("Silver", "DMC", "DeLorean", "Sports");

		System.out.println(b.color + " , " + b.model + " , " + b.make + " , " + b.type);
		System.out.println(h.color + " , " + h.model + " , " + h.make + " , " + h.type);
		System.out.println(f.color + " , " + f.model + " , " + f.make + " , " + f.type);
		System.out.println(d.color + " , " + d.model + " , " + d.make + " , " + d.type);

		b.drive();
		f.drive();
		h.drive();
		d.drive();
	}

}
