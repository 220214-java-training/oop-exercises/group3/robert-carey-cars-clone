package veh;
// Added a Delorean class for fun.
public class Delorean extends Vehicle {



    Delorean() {
        super();
    }
    Delorean(String deloreanColor, String deloreanModel, String deloreanMake, String deloreanType) {
        super(deloreanColor, deloreanModel, deloreanMake, deloreanType);
    }

    void drive() {
        System.out.println("Refill the Mr. Fusion so that we can go 88 MPH and go back in time!");

    }
}